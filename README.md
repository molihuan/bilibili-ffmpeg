# bilibiliFFmpeg

#### 介绍
在做此工具之前做过安卓版的软件，应网友要求做了电脑版，此工具是本人为了方便自用而做的，发布也只为能够帮助到大家以及交流获得工具更优化的建议，因此下载限个人使用，永久免费，软件内部的视频教程完全是为了教程而教程。如用于其他用途甚至用于售卖获利，所有责任自行承担，原作者不承担任何责任。本人从未通过本工具以后也不会从本工具获取任何经济收益，水平太差，代码写的有点乱，大佬勿喷。

#### 说明

软件：VS 2013
目标框架:.NET Framework 4.5 

C#获取文件路径，生成ffmpeg命令后交付ffmpeg执行命令

#### 安装教程

1.  解压即可
2.  xxxx

#### 使用说明

1.  使用应该很简单，实在不知道参考在线视频教程：https://b23.tv/BV1DQ4y1i7gR

#### 软件截图
![主界面](https://images.gitee.com/uploads/images/2021/1023/114755_8b88419e_8420478.png "屏幕截图.png")

#### 参与贡献

1.  Fork 本仓库
2.  【ffmpeg开源项目】:此软件是基于ffmpeg进行开发的。
3.  【CSDN网站】:作者从网站上查找了大量的资料，解决了许多的问题。
4.  【SunnyUI开源项目】使用此项目的解决方案对界面进行美化，项目链接：https://gitee.com/yhuse/SunnyUI


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
