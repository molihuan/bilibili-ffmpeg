﻿
using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bilibiliFFmpeg
{
    public partial class settings : UIForm
    {
        public settings()
        {
            InitializeComponent();
        }

        public void  btn_updata_Click(object sender, EventArgs e)
        {

            string url = "https://blog.csdn.net/molihuan/article/details/120903675";
            ShowWaitForm("正在检测更新，请保持网络畅通");
            //SetWaitFormDescription(UILocalize.SystemProcessing + "50%");
            string res="";
            try
            {
                res= HttpUtil.GetHttpResponse(url, 6000);
            }
            catch (Exception e1)
            {

            }
            

            HideWaitForm();
            

            if (res != null&&!res.Equals(""))
            {
                string pattern = "<meta name=\"keywords\" content=\"哔哩哔哩HLB站缓存合并电脑版链接:(.+)版本:(.+)公告:(.+)\">";
                string parentFphm = Regex.Match(res, pattern).Result("$2");
                string downLink = Regex.Match(res, pattern).Result("$1");
                string announcement = Regex.Match(res, pattern).Result("$3");
                if (parentFphm.Equals(XML.version))
                {
                    ShowWarningTip("已经是最新版本", 1500, true);
                    //MessageBox.Show("已经是最新版本");
                }
                else
                {
                    bool pd= UIMessageDialog.ShowMessageDialog("检测到有新的版本，您是否进行更新？\n（默认密码：MLH）\n"+announcement, "更新提示", true, Style);
                    if (pd)
                    {
                        System.Diagnostics.Process.Start(downLink);
                    }
                    
                }
            }
        }

        

    }
}
