﻿namespace bilibiliFFmpeg
{
    partial class Main
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            this.fileBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.icoImageList = new System.Windows.Forms.ImageList(this.components);
            this.panel_main = new System.Windows.Forms.Panel();
            this.button_merge = new Sunny.UI.UIButton();
            this.dataGridView_nameApath = new Sunny.UI.UIDataGridView();
            this.btn_refresh = new Sunny.UI.UIButton();
            this.btn_checkAll = new Sunny.UI.UIButton();
            this.btn_deselectAll = new Sunny.UI.UIButton();
            this.btn_completeFile = new Sunny.UI.UIButton();
            this.btn_back = new Sunny.UI.UIButton();
            this.btn_choosePath = new Sunny.UI.UIButton();
            this.showPath = new Sunny.UI.UITextBox();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.Menu_start = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_choosPath = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_settings = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_exit = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_about = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_aboutUs = new System.Windows.Forms.ToolStripMenuItem();
            this.button_audio = new Sunny.UI.UIButton();
            this.panel_main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_nameApath)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // icoImageList
            // 
            this.icoImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("icoImageList.ImageStream")));
            this.icoImageList.TransparentColor = System.Drawing.Color.Transparent;
            this.icoImageList.Images.SetKeyName(0, "file_tp.png");
            this.icoImageList.Images.SetKeyName(1, "file_.png");
            // 
            // panel_main
            // 
            this.panel_main.Controls.Add(this.button_audio);
            this.panel_main.Controls.Add(this.button_merge);
            this.panel_main.Controls.Add(this.dataGridView_nameApath);
            this.panel_main.Controls.Add(this.btn_refresh);
            this.panel_main.Controls.Add(this.btn_checkAll);
            this.panel_main.Controls.Add(this.btn_deselectAll);
            this.panel_main.Controls.Add(this.btn_completeFile);
            this.panel_main.Controls.Add(this.btn_back);
            this.panel_main.Controls.Add(this.btn_choosePath);
            this.panel_main.Controls.Add(this.showPath);
            this.panel_main.Controls.Add(this.uiLabel1);
            this.panel_main.Controls.Add(this.menuStrip1);
            this.panel_main.Location = new System.Drawing.Point(5, 38);
            this.panel_main.Name = "panel_main";
            this.panel_main.Size = new System.Drawing.Size(694, 595);
            this.panel_main.TabIndex = 1;
            // 
            // button_merge
            // 
            this.button_merge.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_merge.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.button_merge.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(174)))), ((int)(((byte)(86)))));
            this.button_merge.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.button_merge.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.button_merge.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button_merge.Location = new System.Drawing.Point(365, 550);
            this.button_merge.MinimumSize = new System.Drawing.Size(1, 1);
            this.button_merge.Name = "button_merge";
            this.button_merge.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.button_merge.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(174)))), ((int)(((byte)(86)))));
            this.button_merge.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.button_merge.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.button_merge.Size = new System.Drawing.Size(100, 35);
            this.button_merge.Style = Sunny.UI.UIStyle.Orange;
            this.button_merge.TabIndex = 21;
            this.button_merge.Text = "开始合并";
            this.button_merge.Click += new System.EventHandler(this.button_merge_Click);
            // 
            // dataGridView_nameApath
            // 
            this.dataGridView_nameApath.AllowUserToAddRows = false;
            this.dataGridView_nameApath.AllowUserToDeleteRows = false;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(245)))), ((int)(((byte)(233)))));
            this.dataGridView_nameApath.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dataGridView_nameApath.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView_nameApath.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView_nameApath.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle12.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_nameApath.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridView_nameApath.ColumnHeadersHeight = 32;
            this.dataGridView_nameApath.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(207)))), ((int)(((byte)(151)))));
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_nameApath.DefaultCellStyle = dataGridViewCellStyle13;
            this.dataGridView_nameApath.EnableHeadersVisualStyles = false;
            this.dataGridView_nameApath.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dataGridView_nameApath.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.dataGridView_nameApath.Location = new System.Drawing.Point(12, 123);
            this.dataGridView_nameApath.Name = "dataGridView_nameApath";
            this.dataGridView_nameApath.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(245)))), ((int)(((byte)(233)))));
            dataGridViewCellStyle14.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_nameApath.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            this.dataGridView_nameApath.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridView_nameApath.RowTemplate.Height = 23;
            this.dataGridView_nameApath.SelectedIndex = -1;
            this.dataGridView_nameApath.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_nameApath.ShowGridLine = true;
            this.dataGridView_nameApath.Size = new System.Drawing.Size(666, 421);
            this.dataGridView_nameApath.StripeOddColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(245)))), ((int)(((byte)(233)))));
            this.dataGridView_nameApath.Style = Sunny.UI.UIStyle.Orange;
            this.dataGridView_nameApath.TabIndex = 20;
            this.dataGridView_nameApath.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewItemDoubleClick);
            // 
            // btn_refresh
            // 
            this.btn_refresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_refresh.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.btn_refresh.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(174)))), ((int)(((byte)(86)))));
            this.btn_refresh.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_refresh.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_refresh.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_refresh.Location = new System.Drawing.Point(578, 82);
            this.btn_refresh.MinimumSize = new System.Drawing.Size(1, 1);
            this.btn_refresh.Name = "btn_refresh";
            this.btn_refresh.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.btn_refresh.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(174)))), ((int)(((byte)(86)))));
            this.btn_refresh.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_refresh.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_refresh.Size = new System.Drawing.Size(100, 35);
            this.btn_refresh.Style = Sunny.UI.UIStyle.Orange;
            this.btn_refresh.TabIndex = 19;
            this.btn_refresh.Text = "刷新";
            this.btn_refresh.Click += new System.EventHandler(this.btn_refresh_Click);
            // 
            // btn_checkAll
            // 
            this.btn_checkAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_checkAll.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.btn_checkAll.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(174)))), ((int)(((byte)(86)))));
            this.btn_checkAll.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_checkAll.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_checkAll.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_checkAll.Location = new System.Drawing.Point(463, 82);
            this.btn_checkAll.MinimumSize = new System.Drawing.Size(1, 1);
            this.btn_checkAll.Name = "btn_checkAll";
            this.btn_checkAll.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.btn_checkAll.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(174)))), ((int)(((byte)(86)))));
            this.btn_checkAll.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_checkAll.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_checkAll.Size = new System.Drawing.Size(100, 35);
            this.btn_checkAll.Style = Sunny.UI.UIStyle.Orange;
            this.btn_checkAll.TabIndex = 18;
            this.btn_checkAll.Text = "全选";
            this.btn_checkAll.Click += new System.EventHandler(this.btn_checkAll_Click);
            // 
            // btn_deselectAll
            // 
            this.btn_deselectAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_deselectAll.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.btn_deselectAll.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(174)))), ((int)(((byte)(86)))));
            this.btn_deselectAll.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_deselectAll.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_deselectAll.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_deselectAll.Location = new System.Drawing.Point(351, 82);
            this.btn_deselectAll.MinimumSize = new System.Drawing.Size(1, 1);
            this.btn_deselectAll.Name = "btn_deselectAll";
            this.btn_deselectAll.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.btn_deselectAll.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(174)))), ((int)(((byte)(86)))));
            this.btn_deselectAll.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_deselectAll.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_deselectAll.Size = new System.Drawing.Size(100, 35);
            this.btn_deselectAll.Style = Sunny.UI.UIStyle.Orange;
            this.btn_deselectAll.TabIndex = 17;
            this.btn_deselectAll.Text = "取消全选";
            this.btn_deselectAll.Click += new System.EventHandler(this.btn_deselectAll_Click);
            // 
            // btn_completeFile
            // 
            this.btn_completeFile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_completeFile.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.btn_completeFile.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(174)))), ((int)(((byte)(86)))));
            this.btn_completeFile.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_completeFile.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_completeFile.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_completeFile.Location = new System.Drawing.Point(236, 82);
            this.btn_completeFile.MinimumSize = new System.Drawing.Size(1, 1);
            this.btn_completeFile.Name = "btn_completeFile";
            this.btn_completeFile.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.btn_completeFile.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(174)))), ((int)(((byte)(86)))));
            this.btn_completeFile.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_completeFile.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_completeFile.Size = new System.Drawing.Size(100, 35);
            this.btn_completeFile.Style = Sunny.UI.UIStyle.Orange;
            this.btn_completeFile.TabIndex = 16;
            this.btn_completeFile.Text = "完成文件";
            this.btn_completeFile.Click += new System.EventHandler(this.btn_completeFile_Click);
            // 
            // btn_back
            // 
            this.btn_back.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_back.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.btn_back.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(174)))), ((int)(((byte)(86)))));
            this.btn_back.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_back.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_back.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_back.Location = new System.Drawing.Point(12, 82);
            this.btn_back.MinimumSize = new System.Drawing.Size(1, 1);
            this.btn_back.Name = "btn_back";
            this.btn_back.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.btn_back.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(174)))), ((int)(((byte)(86)))));
            this.btn_back.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_back.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_back.Size = new System.Drawing.Size(100, 35);
            this.btn_back.Style = Sunny.UI.UIStyle.Orange;
            this.btn_back.TabIndex = 15;
            this.btn_back.Text = "返回";
            this.btn_back.Click += new System.EventHandler(this.btn_back_Click);
            // 
            // btn_choosePath
            // 
            this.btn_choosePath.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_choosePath.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.btn_choosePath.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(174)))), ((int)(((byte)(86)))));
            this.btn_choosePath.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_choosePath.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_choosePath.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_choosePath.Location = new System.Drawing.Point(578, 32);
            this.btn_choosePath.MinimumSize = new System.Drawing.Size(1, 1);
            this.btn_choosePath.Name = "btn_choosePath";
            this.btn_choosePath.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.btn_choosePath.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(174)))), ((int)(((byte)(86)))));
            this.btn_choosePath.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_choosePath.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_choosePath.Size = new System.Drawing.Size(100, 35);
            this.btn_choosePath.Style = Sunny.UI.UIStyle.Orange;
            this.btn_choosePath.TabIndex = 14;
            this.btn_choosePath.Text = "选择路径";
            this.btn_choosePath.Click += new System.EventHandler(this.choosePath_Click);
            // 
            // showPath
            // 
            this.showPath.AllowDrop = true;
            this.showPath.ButtonSymbol = 61761;
            this.showPath.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.showPath.FillColor = System.Drawing.Color.White;
            this.showPath.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.showPath.Location = new System.Drawing.Point(119, 34);
            this.showPath.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.showPath.Maximum = 2147483647D;
            this.showPath.Minimum = -2147483648D;
            this.showPath.MinimumSize = new System.Drawing.Size(1, 16);
            this.showPath.Name = "showPath";
            this.showPath.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.showPath.Size = new System.Drawing.Size(444, 29);
            this.showPath.Style = Sunny.UI.UIStyle.Orange;
            this.showPath.TabIndex = 13;
            this.showPath.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.showPath.DragDrop += new System.Windows.Forms.DragEventHandler(this.showPath_DragDrop);
            this.showPath.DragEnter += new System.Windows.Forms.DragEventHandler(this.showPath_DragEnter);
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.uiLabel1.Location = new System.Drawing.Point(8, 34);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(114, 23);
            this.uiLabel1.Style = Sunny.UI.UIStyle.Orange;
            this.uiLabel1.TabIndex = 12;
            this.uiLabel1.Text = "选择合并目录:";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_start,
            this.MenuItem_about});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(694, 29);
            this.menuStrip1.TabIndex = 11;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // Menu_start
            // 
            this.Menu_start.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem_choosPath,
            this.MenuItem_settings,
            this.MenuItem_exit});
            this.Menu_start.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.Menu_start.Name = "Menu_start";
            this.Menu_start.Size = new System.Drawing.Size(54, 25);
            this.Menu_start.Text = "开始";
            // 
            // MenuItem_choosPath
            // 
            this.MenuItem_choosPath.Name = "MenuItem_choosPath";
            this.MenuItem_choosPath.Size = new System.Drawing.Size(176, 26);
            this.MenuItem_choosPath.Text = "选择合并目录";
            this.MenuItem_choosPath.Click += new System.EventHandler(this.MenuItem_choosPath_Click);
            // 
            // MenuItem_settings
            // 
            this.MenuItem_settings.Name = "MenuItem_settings";
            this.MenuItem_settings.Size = new System.Drawing.Size(176, 26);
            this.MenuItem_settings.Text = "设置";
            this.MenuItem_settings.Click += new System.EventHandler(this.MenuItem_settings_Click);
            // 
            // MenuItem_exit
            // 
            this.MenuItem_exit.Name = "MenuItem_exit";
            this.MenuItem_exit.Size = new System.Drawing.Size(176, 26);
            this.MenuItem_exit.Text = "退出";
            this.MenuItem_exit.Click += new System.EventHandler(this.MenuItem_exit_Click);
            // 
            // MenuItem_about
            // 
            this.MenuItem_about.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem_aboutUs});
            this.MenuItem_about.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.MenuItem_about.Name = "MenuItem_about";
            this.MenuItem_about.Size = new System.Drawing.Size(54, 25);
            this.MenuItem_about.Text = "关于";
            // 
            // MenuItem_aboutUs
            // 
            this.MenuItem_aboutUs.Name = "MenuItem_aboutUs";
            this.MenuItem_aboutUs.Size = new System.Drawing.Size(144, 26);
            this.MenuItem_aboutUs.Text = "关于我们";
            this.MenuItem_aboutUs.Click += new System.EventHandler(this.MenuItem_aboutUs_Click);
            // 
            // button_audio
            // 
            this.button_audio.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_audio.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.button_audio.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(174)))), ((int)(((byte)(86)))));
            this.button_audio.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.button_audio.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.button_audio.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button_audio.Location = new System.Drawing.Point(242, 550);
            this.button_audio.MinimumSize = new System.Drawing.Size(1, 1);
            this.button_audio.Name = "button_audio";
            this.button_audio.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.button_audio.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(174)))), ((int)(((byte)(86)))));
            this.button_audio.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.button_audio.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.button_audio.Size = new System.Drawing.Size(100, 35);
            this.button_audio.Style = Sunny.UI.UIStyle.Orange;
            this.button_audio.TabIndex = 22;
            this.button_audio.Text = "提取音频";
            this.button_audio.Click += new System.EventHandler(this.button_audio_Click);
            // 
            // Main
            // 
            this.AllowDrop = true;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(705, 641);
            this.Controls.Add(this.panel_main);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Main";
            this.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.ShowRadius = false;
            this.ShowRect = false;
            this.ShowTitleIcon = true;
            this.Style = Sunny.UI.UIStyle.Orange;
            this.Text = "HLB站缓存合并 V 1.1";
            this.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.Load += new System.EventHandler(this.Main_Load);
            this.panel_main.ResumeLayout(false);
            this.panel_main.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_nameApath)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog fileBrowserDialog;
        private System.Windows.Forms.ImageList icoImageList;
        private System.Windows.Forms.Panel panel_main;
        private Sunny.UI.UITextBox showPath;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UIButton btn_choosePath;
        private Sunny.UI.UIButton btn_refresh;
        private Sunny.UI.UIButton btn_checkAll;
        private Sunny.UI.UIButton btn_deselectAll;
        private Sunny.UI.UIButton btn_completeFile;
        private Sunny.UI.UIButton btn_back;
        private Sunny.UI.UIDataGridView dataGridView_nameApath;
        private Sunny.UI.UIButton button_merge;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem Menu_start;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_choosPath;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_settings;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_exit;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_about;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_aboutUs;
        private Sunny.UI.UIButton button_audio;
    }
}

