﻿namespace bilibiliFFmpeg
{
    partial class AboutUs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutUs));
            this.btn_aboutUsClose = new Sunny.UI.UIButton();
            this.rtb_aboutUs = new Sunny.UI.UIRichTextBox();
            this.SuspendLayout();
            // 
            // btn_aboutUsClose
            // 
            this.btn_aboutUsClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_aboutUsClose.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.btn_aboutUsClose.FillHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(174)))), ((int)(((byte)(86)))));
            this.btn_aboutUsClose.FillPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_aboutUsClose.FillSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_aboutUsClose.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_aboutUsClose.Location = new System.Drawing.Point(221, 378);
            this.btn_aboutUsClose.MinimumSize = new System.Drawing.Size(1, 1);
            this.btn_aboutUsClose.Name = "btn_aboutUsClose";
            this.btn_aboutUsClose.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.btn_aboutUsClose.RectHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(223)))), ((int)(((byte)(174)))), ((int)(((byte)(86)))));
            this.btn_aboutUsClose.RectPressColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_aboutUsClose.RectSelectedColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(137)))), ((int)(((byte)(43)))));
            this.btn_aboutUsClose.Size = new System.Drawing.Size(100, 35);
            this.btn_aboutUsClose.Style = Sunny.UI.UIStyle.Orange;
            this.btn_aboutUsClose.TabIndex = 2;
            this.btn_aboutUsClose.Text = "朕已阅";
            this.btn_aboutUsClose.Click += new System.EventHandler(this.btn_aboutUsClose_Click);
            // 
            // rtb_aboutUs
            // 
            this.rtb_aboutUs.AutoScroll = true;
            this.rtb_aboutUs.AutoWordSelection = true;
            this.rtb_aboutUs.FillColor = System.Drawing.Color.White;
            this.rtb_aboutUs.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.rtb_aboutUs.Location = new System.Drawing.Point(60, 67);
            this.rtb_aboutUs.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rtb_aboutUs.MinimumSize = new System.Drawing.Size(1, 1);
            this.rtb_aboutUs.Name = "rtb_aboutUs";
            this.rtb_aboutUs.Padding = new System.Windows.Forms.Padding(2);
            this.rtb_aboutUs.ReadOnly = true;
            this.rtb_aboutUs.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.rtb_aboutUs.Size = new System.Drawing.Size(422, 303);
            this.rtb_aboutUs.Style = Sunny.UI.UIStyle.Orange;
            this.rtb_aboutUs.TabIndex = 3;
            this.rtb_aboutUs.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            this.rtb_aboutUs.WordWrap = true;
            // 
            // AboutUs
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(539, 434);
            this.Controls.Add(this.rtb_aboutUs);
            this.Controls.Add(this.btn_aboutUsClose);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "AboutUs";
            this.RectColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.ShowIcon = true;
            this.ShowRadius = false;
            this.ShowRect = false;
            this.Style = Sunny.UI.UIStyle.Orange;
            this.Text = "关于我们";
            this.TitleColor = System.Drawing.Color.FromArgb(((int)(((byte)(220)))), ((int)(((byte)(155)))), ((int)(((byte)(40)))));
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIButton btn_aboutUsClose;
        private Sunny.UI.UIRichTextBox rtb_aboutUs;


    }
}