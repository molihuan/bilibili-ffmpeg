﻿using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bilibiliFFmpeg
{
    public partial class AboutUs : UIForm
    {
        public AboutUs()
        {
            InitializeComponent();
            initView();
        }

        private void initView()
        {
            rtb_aboutUs.Text = "\t写在前面：在做此工具之前做过安卓版的软件，应网友要求做了电脑版，此工具是本人为了方便自用而做的，发布也只为能够帮助到大家以及交流获得工具更优化的建议，因此下载限个人使用，永久免费，软件内部的视频教程完全是为了教程而教程。如用于其他用途甚至用于售卖获利，所有责任自行承担，原作者不承担任何责任。本人从未通过本工具以后也不会从本工具获取任何经济收益，水平太差，大佬勿喷。\n\t注意：目前只支持新版的缓存格式m4s,暂不支持blv格式。\n\t在线视频教程：https://b23.tv/BV1DQ4y1i7gR \n\n 本《声明》包括:特别鸣谢和《软件协议》。\n\t一、特别鸣谢\n\t  为本软件开发提供重要帮助的开源项目和用户:\n\t  【ffmpeg开源项目】:此软件是基于ffmpeg进行开发的。\n\t  【CSDN】:作者从网站上查找了大量的资料，解决了许多的问题。\n\t  【SunnyUI开源项目】使用此项目的解决方案对界面进行美化，项目链接：https://gitee.com/yhuse/SunnyUI \n\t  非常感谢以下网友帮助测试反馈Bug并提出建议:\n\t  【@B站：A7RA3L小李y】\n\t  【@B站：啤酒配尖椒】\n\t  【@B站：林笙不是林笙】\n\t  【@娜】:设计软件图标，提供部分图片资源支持。\n\t  等等。\n\t《软件协议》描述我们与您之间关于本软件许可使用及相关方面的权利义务。请您务必审慎阅读、充分理解各条款内容，并选择接受或不接受（未成年人应在法定监护人陪同下审阅）。除非您接受本《软件协议》条款，否则您无权下载、安装或使用本软件及其相关服务。您的安装、使用行为将视为对本《协议》的接受，并同意接受本《协议》各项条款的约束。\n\t二、《软件协议》\n\t  【用户禁止行为】除非我们书面许可，您不得从事下列行为：\n\t   （1）用于商业用途。\n\t  【法律责任与免责】\n\t   （1）本软件仅用于学习和技术交流，不可用于商业用途，造成的损失原作者不承担任何责任。\n\t   （2）可以进行二次修改，软件已经开源，你可以在Gitee下载源码：https://gitee.com/molihuan/bilibili-ffmpeg \n二次修改后的软件造成您的损失原作者不承担任何责任。\n\t   （3）如此软件侵犯到您的权益请联系我QQ：1492906929@qq.com，我会进行处理，给您带来不便我深感抱歉。\n\t  【其他条款】\n\t   （1）电子文本形式的授权协议如同双方书面签署的协议一样，具有完全的和等同的法律效力。您使用本软件或本服务即视为您已阅读并同意受本协议的约束。协议许可范围以外的行为，将直接违反本授权协议并构成侵权，我们有权随时终止授权，责令停止损害，并保留追究相关责任的权力。\n\t   （2）我们有权在必要时修改本协议条款。您可以在本软件的最新版本中查阅相关协议条款。本协议条款变更后，如果您继续使用本软件，即视为您已接受修改后的协议。如果您不接受修改后的协议，应当停止使用本软件。\n\t   （3）我们保留对本协议的最终解释权。";
        }

        private void btn_aboutUsClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
