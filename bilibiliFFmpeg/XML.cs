﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;


namespace bilibiliFFmpeg
{
    class XML
    {
        public static List<xmlData> xmldata = new List<xmlData>();//存储
        public static string version = "1.1";


        public static bool createXml(string path)
        {
            XmlTextWriter xw = new XmlTextWriter(path,null);
            xw.WriteStartDocument();
            xw.WriteStartElement("root");
            xw.WriteElementString("version", version);
            xw.WriteElementString("updataPrompt","true");
            xw.WriteElementString("vedio", "在线视频教程:https://b23.tv/BV1DQ4y1i7gR ");
            xw.WriteEndElement();
            xw.WriteEndDocument();
            xw.Flush();
            xw.Close();
            return true;
        }

        public static string readXml(string path,string key)
        {

            XmlDataDocument xd = new XmlDataDocument();
            xd.Load(path);
            XmlNodeList nodes = xd.SelectNodes("root/" + key);
            return nodes[0].InnerText;

        }


        public static bool changeXml(string path, string key,string value)
        {
            XmlDataDocument xd = new XmlDataDocument();
            xd.Load(path);
            XmlNodeList nodes = xd.SelectNodes("root/" + key);
            nodes[0].InnerText = value;
            xd.Save(path);
            return true;
        }



        public static void clearData()
        {
            xmldata.Clear();
        }

    }


    class xmlData
    {
        private static string key;

        public static string Key
        {
            get { return xmlData.key; }
            set { xmlData.key = value; }
        }
        private static string content;

        public static string Content
        {
            get { return xmlData.content; }
            set { xmlData.content = value; }
        }
    }
}
