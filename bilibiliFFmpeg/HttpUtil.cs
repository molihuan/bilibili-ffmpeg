﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace bilibiliFFmpeg
{
    class HttpUtil
    {
        ///
        /// Get请求
        /// 
        /// 字符串
        public static string GetHttpResponse(string url, int Timeout)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "text/html;charset=UTF-8";

            request.Timeout = Timeout;

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream myResponseStream = response.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8"));
            string retString = myStreamReader.ReadToEnd();
            myStreamReader.Close();
            myResponseStream.Close();
            if (retString.Equals("")) {
                retString = "error";
            }
            return retString;
        }



        public static string PostWebRequest(string postUrl, string paramData, Encoding dataEncode)
        {
            string ret = string.Empty;
            try
            {
                HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(postUrl);
                webReq.Method = "POST";
                webReq.ContentType = "application/x-www-form-urlencoded";//只能使用这个提交不然后台获取不到参数
                byte[] byteArray = Encoding.UTF8.GetBytes(paramData); //转化只能使用utf-8不然后台获取不到参数
                int length = byteArray.Length;
                webReq.ContentLength = length;
                Stream newStream = webReq.GetRequestStream();
                newStream.Write(byteArray, 0, length);//写入参数
                newStream.Close();
                HttpWebResponse response = (HttpWebResponse)webReq.GetResponse();
                StreamReader sr = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                ret = sr.ReadToEnd();
                sr.Close();
                response.Close();
                newStream.Close();
            }
            catch (Exception ex)
            {
            }
            return ret;
        }



        /// <summary>
        /// Post请求 参数以UrlEncode方式发送
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="dic">参数对象 类型 Object </param>
        /// <param name="result">请求结果数据</param>
        /// <param name="token">Token,不需要Token时 无需传参</param>
        /// <param timeOut="token">请求过期时间</param>
        /// <returns></returns>
        public static string postForUrlEcode(string url, Dictionary<String, String> dic, String token = "false", int timeOut = 5000)
        {
            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                req.Method = "POST";
                req.Timeout = timeOut;
                if (token != "false")
                {
                    req.Headers["Authorization"] = "Bearer " + token;
                }
                req.ContentType = "application/x-www-form-urlencoded";
                #region 添加Post 参数
                StringBuilder builder = new StringBuilder();
                int i = 0;
                foreach (var item in dic)
                {
                    if (i > 0)
                        builder.Append("&");
                    builder.AppendFormat("{0}={1}", item.Key, item.Value);
                    i++;
                }
                byte[] data = Encoding.UTF8.GetBytes(builder.ToString());
                req.ContentLength = data.Length;
                using (Stream reqStream = req.GetRequestStream())
                {
                    reqStream.Write(data, 0, data.Length);
                    reqStream.Close();
                }
                #endregion
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
                Stream stream = resp.GetResponseStream();
                string result="null";
                //获取响应内容
                using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                {
                    result = reader.ReadToEnd();
                }
                //return result;
                return result;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }








    }


}
